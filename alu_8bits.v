module alu_8bits (	input [7:0]a,
                     input [7:0]b,
                     input [1:0]sel,
                     output reg[7:0]r,
                     output reg[3:0]status); // zero, negative, overflow, carry

 		
    always @(a, b, sel)
    begin
		  r <= 2'h00;
		  status <= 1'h0;
        case(sel)
         2'b00 : begin 
								{status[0],r}<=a+b;  //de haber carry lo guardo en el status
								status[1]<=(a[7] & b[7] & ~r[7]) | (~a[7] & ~b[7] & r[7]);
						end
         2'b01 : begin 
								{status[2],r}<=a-b; // status[2] indica que a < b
								if (a==b)
									status[3]<=1;
					  end
         2'b10 : r<=a&b;
         2'b11 : r<=a|b;
       endcase
     end
 endmodule