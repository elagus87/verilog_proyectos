`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:07:06 09/08/2017 
// Design Name: 
// Module Name:    alu_completa 
// Project Name: 	 micro
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu_completa(
    input clk,
    input [4:0] inst,  /*recibe del bus de instrucciones [i7,i6,i2,i1,i0]*/
    input [7:0] x,
    input [7:0] i,
	 input rst,
    output [3:0] status,
    output reg[7:0] acc
    );

	reg [7:0]acc_reg = 8'h00;
	reg [7:0]acc_next= 8'h00;
	wire [7:0]r_alu;
	reg [1:0]mux_dir_sel= 2'b00;
	reg [7:0]b=8'h00;
	
	// logica de estado futuro
	 always @(posedge rst,posedge clk) begin
		if(rst)begin 
			acc_reg <= 8'h00;
		end
		else begin
      acc_reg <= acc_next;
		end
    end
	 

		
	// logica preparatoria del estado futuro
		always @(mux_dir_sel,i,x,inst,r_alu,acc_reg) begin
			// logica de direccionamiento hacia el acumulador
			 mux_dir_sel[0] <= (inst[4]&inst[3]) |( (~inst[4]&~inst[3])&(inst[0]|inst[1] ));
			 mux_dir_sel[1] <= (inst[4]&inst[3]) |( (~inst[4]&~inst[3])&(inst[0]|~inst[1] ));
			case (mux_dir_sel)	//mux previo al registro
			2'b00  : begin
							acc_next <= r_alu;
						end
			2'b01  : begin
							acc_next <= i;
						end
			2'b10  : begin
							acc_next <= x;
						end
			2'b11  : begin
							acc_next <= acc_reg;
						end
			endcase
			acc <= acc_reg; //salida
		end
		
		// logica preparatoria del dato a la ALU
		always @(inst[2],x,i,b) begin
			case (inst[2])	//mux previo al registro
			1'b0  : begin
							b <= x;
						end
			1'b1  : begin
							b <= i;
						end
			endcase
		end
		// instancia de la ALU de 8 bits
		alu_8bits ALU(
							.a(acc_reg),
							.b(b),
							.sel( {inst[4],inst[1],inst[0]} ),
							.clk(clk),
							.rst(rst),
							.r(r_alu),
							.status(status)
						  );
						  
endmodule
