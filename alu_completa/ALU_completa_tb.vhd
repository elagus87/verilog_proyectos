--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:55:15 09/12/2017
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/verilog_proyectos/alu_completa/ALU_completa_tb.vhd
-- Project Name:  alu_completa
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: alu_completa
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ALU_completa_tb IS
END ALU_completa_tb;
 
ARCHITECTURE behavior OF ALU_completa_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu_completa
    PORT(
         clk : IN  std_logic;
         inst : IN  std_logic_vector(4 downto 0);
         x : IN  std_logic_vector(7 downto 0);
         i : IN  std_logic_vector(7 downto 0);
         rst : IN  std_logic;
         status : OUT  std_logic_vector(3 downto 0);
         acc : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal inst : std_logic_vector(4 downto 0) := (others => '0');
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal i : std_logic_vector(7 downto 0) := (others => '0');
   signal rst : std_logic := '0';

 	--Outputs
   signal status : std_logic_vector(3 downto 0);
   signal acc : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
	-- constante de tiempo
	constant interval : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu_completa PORT MAP (
          clk => clk,
          inst => inst,
          x => x,
          i => i,
          rst => rst,
          status => status,
          acc => acc
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		rst<='1';
		wait for interval;
		rst<='0';
		x<="00000010";--2
		i<="00000001";--1

		inst(4 downto 3)<="01"; --aritmetica y ALU en el mux
		inst(2)<='0'; -- con x
		inst(1)<='0'; --sin carry
		inst(0)<='1'; --suma
		wait for interval; report "fin de la suma";
		inst(0)<='0'; --resta
		wait for interval; report "fin de la resta";
		
		inst(4 downto 3)<="10"; --logica
		inst(2)<='1'; 				--con i
		inst(1 downto 0)<="00"; --Nor
		wait for interval; report "fin de la Nor";
		inst(1 downto 0)<="01"; --Nand
		wait for interval; report "fin de la Nand";
		inst(1 downto 0)<="10"; --Xor
		wait for interval; report "fin de la Xor";
		inst(1 downto 0)<="11"; --Xnor
		wait for interval; report "fin de la Xnor";
		
		inst(4 downto 3)<="00"; --elije i
		inst(1)<='0';
		wait for interval; report "seleccion de i";
		inst(4 downto 3)<="00"; --elije x
		inst(1)<='1';
		wait for interval; report "seleccion de x";
		inst(4 downto 3)<="11"; --restiene
		wait for interval; report "retención";
		assert false
		report "fin de la simulacion"
		severity failure;
		

   end process;

END;
