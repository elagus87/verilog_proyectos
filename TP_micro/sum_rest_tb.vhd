--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:13:40 09/12/2017
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/verilog_proyectos/sum_rest_8/sum_rest_tb.vhd
-- Project Name:  sum_rest_8
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sum_rest_8
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY sum_rest_tb IS
END sum_rest_tb;
 
ARCHITECTURE behavior OF sum_rest_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sum_rest_8
    PORT(
         a : IN  std_logic_vector(7 downto 0);
         b : IN  std_logic_vector(7 downto 0);
         ci : IN  std_logic;
         add : IN  std_logic;
         r : OUT  std_logic_vector(7 downto 0);
         ov : OUT  std_logic;
         co : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal b : std_logic_vector(7 downto 0) := (others => '0');
   signal ci : std_logic := '0';
   signal add : std_logic := '0';

 	--Outputs
   signal r : std_logic_vector(7 downto 0);
   signal ov : std_logic;
   signal co : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant interval : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sum_rest_8 PORT MAP (
          a => a,
          b => b,
          ci => ci,
          add => add,
          r => r,
          ov => ov,
          co => co
        );


   -- Stimulus process
   stim_proc: process
		variable count: unsigned(9 downto 0):=(others=>'0');
   begin		
     ci<='0';
	  add<='1';
	  while count(9)='0' loop
			  a<=std_logic_vector(count(7 downto 0));
			  b<=std_logic_vector(count(7 downto 0));
			  wait for interval;
			  count:=count+64;
	  end loop;
	  count:=(others=>'0');
	  
     ci<='0';
	  add<='0';
	  while count(9)='0' loop
			  a<=std_logic_vector(count(7 downto 0));
			  b<=std_logic_vector(count(7 downto 0));
			  wait for interval;
			  count:=count+64;
	  end loop;
	  count:=(others=>'0');
	  
     ci<='1';
	  add<='1';
	  while count(9)='0' loop
			  a<=std_logic_vector(count(7 downto 0));
			  b<=std_logic_vector(count(7 downto 0));
			  wait for interval;
			  count:=count+64;
	  end loop;
	  count:=(others=>'0');
	  
     ci<='1';
	  add<='0';
	  while count(9)='0' loop
			  a<=std_logic_vector(count(7 downto 0));
			  b<=std_logic_vector(count(7 downto 0));
			  wait for interval;
			  count:=count+64;
	  end loop;
	  count:=(others=>'0');
	  
	  assert false
	  severity failure;
	  report "fin";
   end process;

END;
