`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:46:23 08/18/2017 
// Design Name: 
// Module Name:    mux_agus 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_agus #(parameter N=8)(
    input [N-1:0] x0,
    input [N-1:0] x1,
    input sel,
    output [N-1:0] y
    );

	assign y = sel? x0:x1;
endmodule
