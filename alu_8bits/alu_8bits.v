module alu_8bits (  input [7:0]a,
                    input [7:0]b,
                    input [2:0]sel,
                    input clk,
						  input rst,
                    output reg[7:0]r,
                    output reg[3:0]status); // carry, zero, negative, overflow.


     reg cy;
     always @(posedge rst,posedge clk)
		  begin
		  if(rst) begin 
				cy<=1'b0;
        end
		  else begin
			  cy<=status[0];
		  end
	  end
     


    always @(a,b,sel,cy)
    begin
          r <= 8'b00000000;
          status <= 1'h0;
            // parte logica
         if(sel==3'b100) begin r<=~(a&b);end
         else if(sel==3'b101) begin r<=~(a|b);end
            else if(sel==3'b110) begin r<=(a^b);end
         else if(sel==3'b111) begin r<=~(a^b);end
            
            else begin
               if (~sel[0]) begin
                    {status[0],r}<=a+b+(cy & sel[1]);
                    status[3]<=(a[7] & b[7] & ~r[7]) | (~a[7] & ~b[7] & r[7]);
                end
                else begin
                    {status[0],r}<=a-b-(cy & sel[1]);
                    status[3]<=(~a[7] & b[7] & r[7]) | (a[7] & ~b[7] & ~r[7]);
                end
            end

            // utilisando el sumador de 8 bits se hace toda la aritmetica

            if(r==8'b00000000) begin status[1]<=1;end

            status[2]<=r[7];
      end

 endmodule
 