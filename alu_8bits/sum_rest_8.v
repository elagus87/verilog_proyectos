`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:11:11 09/12/2017 
// Design Name: 
// Module Name:    sum_rest_8 
// Project Name:   micro
// Target Devices: 
// Tool versions: 
// Description: sumador restador completo
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sum_rest_8(
    input [7:0] a,
    input [7:0] b,
    input ci,
    input add,
    output reg[7:0] r,
    output reg ov,
    output reg co
    );

	always@(*)
		begin
		      if (add) begin
					{co,r}=a+b+ci;
					ov=(a[7] & b[7] & ~r[7]) | (~a[7] & ~b[7] & r[7]);
				end
				else begin
					{co,r}=a-b-ci;
					ov=(~a[7] & b[7] & r[7]) | (a[7] & ~b[7] & ~r[7]);
				end
	end

endmodule
