<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="I0" />
        <signal name="XLXN_4" />
        <signal name="I1" />
        <signal name="XLXN_6" />
        <signal name="carry" />
        <signal name="overflow" />
        <signal name="XLXN_9" />
        <signal name="b" />
        <signal name="a" />
        <signal name="clk" />
        <signal name="r(7:0)" />
        <signal name="XLXN_15(7:0)" />
        <signal name="XLXN_16(7:0)" />
        <signal name="zero" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="I1" />
        <port polarity="Output" name="carry" />
        <port polarity="Output" name="overflow" />
        <port polarity="Input" name="b" />
        <port polarity="Input" name="a" />
        <port polarity="Input" name="clk" />
        <port polarity="Output" name="r(7:0)" />
        <port polarity="Output" name="zero" />
        <blockdef name="adsu8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="ifd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-140" x1="84" />
            <line x2="84" y1="-116" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
        </blockdef>
        <blockdef name="comp8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="320" y1="-224" y2="-224" x1="384" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <block symbolname="adsu8" name="XLXI_5">
            <blockpin signalname="a" name="A(7:0)" />
            <blockpin signalname="XLXN_2" name="ADD" />
            <blockpin signalname="b" name="B(7:0)" />
            <blockpin signalname="XLXN_4" name="CI" />
            <blockpin signalname="XLXN_6" name="CO" />
            <blockpin signalname="overflow" name="OFL" />
            <blockpin signalname="r(7:0)" name="S(7:0)" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="I0" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="carry" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="ifd" name="XLXI_13">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_6" name="D" />
            <blockpin signalname="carry" name="Q" />
        </block>
        <block symbolname="comp8" name="XLXI_14">
            <blockpin signalname="r(7:0)" name="A(7:0)" />
            <blockpin signalname="XLXN_16(7:0)" name="B(7:0)" />
            <blockpin signalname="zero" name="EQ" />
        </block>
        <block symbolname="constant" name="XLXI_7">
            <attr value="00" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_16(7:0)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="768" y="1008" name="XLXI_5" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="768" y1="944" y2="944" x1="736" />
        </branch>
        <instance x="512" y="976" name="XLXI_11" orien="R0" />
        <branch name="I0">
            <wire x2="400" y1="944" y2="944" x1="384" />
            <wire x2="416" y1="944" y2="944" x1="400" />
            <wire x2="512" y1="944" y2="944" x1="416" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="768" y1="560" y2="560" x1="736" />
        </branch>
        <instance x="480" y="656" name="XLXI_12" orien="R0" />
        <branch name="I1">
            <wire x2="480" y1="592" y2="592" x1="384" />
        </branch>
        <instance x="1632" y="1200" name="XLXI_13" orien="R0" />
        <branch name="XLXN_6">
            <wire x2="1632" y1="944" y2="944" x1="1216" />
        </branch>
        <branch name="carry">
            <wire x2="400" y1="448" y2="528" x1="400" />
            <wire x2="480" y1="528" y2="528" x1="400" />
            <wire x2="2080" y1="448" y2="448" x1="400" />
            <wire x2="2080" y1="448" y2="944" x1="2080" />
            <wire x2="2192" y1="944" y2="944" x1="2080" />
            <wire x2="2208" y1="944" y2="944" x1="2192" />
            <wire x2="2080" y1="944" y2="944" x1="2016" />
        </branch>
        <branch name="overflow">
            <wire x2="1440" y1="880" y2="880" x1="1216" />
            <wire x2="1456" y1="880" y2="880" x1="1440" />
            <wire x2="1456" y1="800" y2="880" x1="1456" />
            <wire x2="2192" y1="800" y2="800" x1="1456" />
            <wire x2="2208" y1="800" y2="800" x1="2192" />
        </branch>
        <branch name="b">
            <wire x2="400" y1="816" y2="816" x1="384" />
            <wire x2="768" y1="816" y2="816" x1="400" />
        </branch>
        <branch name="a">
            <wire x2="768" y1="688" y2="688" x1="384" />
        </branch>
        <iomarker fontsize="28" x="384" y="688" name="a" orien="R180" />
        <iomarker fontsize="28" x="384" y="816" name="b" orien="R180" />
        <iomarker fontsize="28" x="384" y="944" name="I0" orien="R180" />
        <iomarker fontsize="28" x="384" y="592" name="I1" orien="R180" />
        <iomarker fontsize="28" x="2208" y="944" name="carry" orien="R0" />
        <iomarker fontsize="28" x="2208" y="800" name="overflow" orien="R0" />
        <branch name="clk">
            <wire x2="1632" y1="1072" y2="1072" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="1072" name="clk" orien="R180" />
        <branch name="r(7:0)">
            <wire x2="1328" y1="752" y2="752" x1="1216" />
            <wire x2="2448" y1="752" y2="752" x1="1328" />
            <wire x2="1328" y1="752" y2="1208" x1="1328" />
            <wire x2="1328" y1="1208" y2="1216" x1="1328" />
            <wire x2="1328" y1="1216" y2="1392" x1="1328" />
            <wire x2="1456" y1="1392" y2="1392" x1="1328" />
        </branch>
        <iomarker fontsize="28" x="2448" y="752" name="r(7:0)" orien="R0" />
        <instance x="1456" y="1712" name="XLXI_14" orien="R0" />
        <branch name="XLXN_16(7:0)">
            <wire x2="1328" y1="1584" y2="1584" x1="1232" />
            <wire x2="1456" y1="1584" y2="1584" x1="1328" />
        </branch>
        <branch name="zero">
            <wire x2="2080" y1="1488" y2="1488" x1="1840" />
        </branch>
        <instance x="1088" y="1552" name="XLXI_7" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2080" y="1488" name="zero" orien="R0" />
    </sheet>
</drawing>