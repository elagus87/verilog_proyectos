--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:59:04 09/07/2017
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/verilog_proyectos/alu_8bits/alu_tb.vhd
-- Project Name:  alu_8bits
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: alu_8bits
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY alu_tb IS
END alu_tb;
 
ARCHITECTURE behavior OF alu_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu_8bits
    PORT(
         a : IN  std_logic_vector(7 downto 0);
         b : IN  std_logic_vector(7 downto 0);
         sel : IN  std_logic_vector(2 downto 0);
			clk: IN std_logic;
			rst: IN std_logic;
         r : OUT  std_logic_vector(7 downto 0);
         status : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
	signal clk : std_logic:='0';
	signal rst : std_logic:='0';
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal b : std_logic_vector(7 downto 0) := (others => '0');
   signal sel : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal r : std_logic_vector(7 downto 0);
   signal status : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
	constant interval : time := 50 ns;
	constant clk_period: time :=10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu_8bits PORT MAP (
          a => a,
          b => b,
			 clk=>clk,
			 rst=>rst,
          sel => sel,
          r => r,
          status => status
        );
	clock:process
	begin
		clk<='1';
		wait for clk_period;
		clk<='0';
		wait for clk_period;
	end process;
		


   -- Stimulus process
   stim_proc: process
   begin		
		rst<='1';
		wait for interval;
		rst<='0';
		a<="00000010";--2
		b<="00000001";--1
		sel<="000"; --suma
		wait for interval;
		sel<="001"; --resta
		wait for interval;
		sel<="100"; --Nand
		wait for interval;
		sel<="101"; --Nor
		wait for interval;
		sel<="110"; --Xor
		wait for interval;
		sel<="111"; --Xnor
		wait for interval;
		
		b<="00000010";--2
		a<="00000001";--1
		sel<="001"; --resta forzando negative
		wait for interval;
		b<="00000010";--2
		a<="00000010";--2
		sel<="001"; --resta forzando zero
		wait for interval;
		a<="10000000";-- -128
		b<="00000001";-- 1
		sel<="001"; --resta forzando overflow
		wait for interval;
		b<="11111111";--255
		a<="00000010";--2
		sel<="000"; --suma forzando carry
		wait for interval;
		sel<="010"; --suma con y forzando carry
		wait for interval;
		sel<="011"; --resta con y forzando carry
		wait for interval;
		-- Final de la simulación
		assert false
		report "Simulación Completa"
		severity failure;
   end process;

END;
