`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:49:28 08/18/2017 
// Design Name: 
// Module Name:    and_or_agus 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module and_xor_agus(
    input wire x1,
    input wire x2,
    output wire y1,
	 output wire y2
    );

	assign y1 = x1 & x2;
	assign y2 = x1 ^ x2;
	
endmodule
