<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a(7:0)" />
        <signal name="b(7:0)" />
        <signal name="s(7:0)" />
        <signal name="ov" />
        <signal name="co" />
        <signal name="add" />
        <signal name="ci" />
        <port polarity="Input" name="a(7:0)" />
        <port polarity="Input" name="b(7:0)" />
        <port polarity="Output" name="s(7:0)" />
        <port polarity="Output" name="ov" />
        <port polarity="Output" name="co" />
        <port polarity="Input" name="add" />
        <port polarity="Input" name="ci" />
        <blockdef name="adsu8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <block symbolname="adsu8" name="XLXI_1">
            <blockpin signalname="a(7:0)" name="A(7:0)" />
            <blockpin signalname="add" name="ADD" />
            <blockpin signalname="b(7:0)" name="B(7:0)" />
            <blockpin signalname="ci" name="CI" />
            <blockpin signalname="co" name="CO" />
            <blockpin signalname="ov" name="OFL" />
            <blockpin signalname="s(7:0)" name="S(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1120" y="1360" name="XLXI_1" orien="R0" />
        <branch name="a(7:0)">
            <wire x2="1120" y1="1040" y2="1040" x1="928" />
        </branch>
        <branch name="b(7:0)">
            <wire x2="1120" y1="1168" y2="1168" x1="928" />
        </branch>
        <branch name="s(7:0)">
            <wire x2="1776" y1="1104" y2="1104" x1="1568" />
        </branch>
        <branch name="ov">
            <wire x2="1776" y1="1232" y2="1232" x1="1568" />
        </branch>
        <branch name="co">
            <wire x2="1776" y1="1296" y2="1296" x1="1568" />
        </branch>
        <branch name="add">
            <wire x2="1120" y1="1296" y2="1296" x1="928" />
        </branch>
        <branch name="ci">
            <wire x2="1120" y1="912" y2="912" x1="928" />
        </branch>
        <iomarker fontsize="28" x="928" y="912" name="ci" orien="R180" />
        <iomarker fontsize="28" x="928" y="1040" name="a(7:0)" orien="R180" />
        <iomarker fontsize="28" x="928" y="1168" name="b(7:0)" orien="R180" />
        <iomarker fontsize="28" x="928" y="1296" name="add" orien="R180" />
        <iomarker fontsize="28" x="1776" y="1296" name="co" orien="R0" />
        <iomarker fontsize="28" x="1776" y="1232" name="ov" orien="R0" />
        <iomarker fontsize="28" x="1776" y="1104" name="s(7:0)" orien="R0" />
    </sheet>
</drawing>